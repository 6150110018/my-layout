package com.example.proflie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class prof extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prof);
        Button b = findViewById(R.id.btn_back);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(prof.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
    public void Facebook (View view){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/thanakrit.peandee/"));
        startActivity(i);
    }
}